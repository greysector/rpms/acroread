# Prevent the plugins from being stripped and disabled
%define __os_install_post /usr/lib/rpm/brp-compress || :

# Don't create a debuginfo package.
%define debug_package %{nil}

%define libldap %(%{_bindir}/readlink %{_libdir}/libldap.so)
%define liblber %(%{_bindir}/readlink %{_libdir}/liblber.so)

Summary: Adobe Reader for viewing PDF files
Name: acroread
Version: 9.5.5
Release: 7%{?dist}
License: Adobe
Group: Applications/Publishing
URL: http://www.adobe.com/products/acrobat/readermain.html
Source0: http://ardownload.adobe.com/pub/adobe/reader/unix/9.x/%{version}/enu/AdbeRdr%{version}-1_i486linux_enu.tar.bz2
Source1: http://www.adobe.com/products/eulas/pdfs/Reader_Player_AIR_WWEULA-Combined-20080204_1313.pdf
Patch0: %{name}-desktop.patch
BuildArch: i686
BuildRequires: %{_bindir}/execstack
BuildRequires: chrpath
BuildRequires: desktop-file-utils
BuildRequires: openldap-devel(x86-32)
Obsoletes: acrobat <= %{version}, AdobeReader_enu <= %{version}
# Adobe/Reader9/Reader/intellinux/plug_ins3d/prc/MyriadCAD.otf
Provides: adobe-myriadcad-fonts = 1.023
Provides: bundled(libcurl) = 7.15.0
Provides: bundled(libicu) = 3.6
Provides: bundled(openssl) = 0.9.8j
Requires: ca-certificates
Requires: hicolor-icon-theme
%if "%{libldap}" != ""
Requires: %{libldap}
Requires: %{liblber}
%endif

%global __provides_exclude_from ^%{_libdir}/acroread/Adobe/Reader9/Reader/intellinux/lib.*\\.so
%global __requires_exclude ^lib\(ACE\|AdobeXMP\|adobelinguistic\|AGM\|AXE8SharedExpat\|AXSLE\|BIB\|BIBUtils\|CoolType\|eggtrayicon\|extendscript\|JP2K\|ResAccess\|sccore\|WRServices)\\.so\|lib(crypto\|ssl)\\.so\\.0\\.9\\.8\|libcurl.so.3\|libicu(cnv\|data\|i18n\|uc)\\.so\\.36$

%description
Adobe Reader is part of the Adobe Acrobat family of software,
which lets you view, distribute, and print documents in Portable
Document Format (PDF)--regardless of the computer, operating system,
fonts, or application used to create the original file.

PDF files retain all the formatting, fonts, and graphics of the
original document, and virtually any PostScript(TM) document can
be converted into a PDF file. Adobe Acrobat Reader has a plug-in
for web browsers supporting NPAPI to view PDF files inline.

%prep
%setup -q -c

%build
echo Nothing to build

%install
install -d -m0755 %{buildroot}{%{_bindir},%{_libdir}/acroread}
for f in COMMON ILINXR ; do
    tar -xf AdobeReader/${f}.TAR -C %{buildroot}%{_libdir}/acroread/
done
ln -s %{_libdir}/acroread/Adobe/Reader9/bin/acroread %{buildroot}%{_bindir}/acroread

pushd %{buildroot}%{_libdir}/acroread/Adobe/Reader9/Reader/intellinux
# Apparently, there's a hidden dependency on openldap in PPKLite.api plugin 
ln -s %{libldap} lib/libldap.so
ln -s %{liblber} lib/liblber.so
for file in \
  plug_ins/AcroForm/PMP/lib{datamatrix,pdf417,qrcode}pmp.pmp \
  lib/lib{sccore,AdobeXMP,extendscript}.so \
; do
  chrpath -d $file
done
for file in \
  plug_ins/{Annots,PPKLite}.api \
  lib/lib{sccore.so,crypto.so.0.9.8} \
  bin/acroread \
; do
  execstack -c $file
done
# these depend on obsolete pangox library
rm bin/SynchronizerApp-binary lib/libeggtrayicon.so
rm bin/xdg-*
rm -r mozilla
popd

cp -p %{SOURCE1} .

pushd %{buildroot}%{_libdir}/acroread/Adobe/Reader9
pushd Resource
patch -p1 Support/AdobeReader.desktop %{PATCH0}
install -d -m0755 %{buildroot}%{_datadir}/applications
desktop-file-install --delete-original \
                     --dir %{buildroot}%{_datadir}/applications \
                     Support/AdobeReader.desktop
pushd Icons
for dir in 16x16 20x20 22x22 24x24 32x32 36x36 48x48 64x64 96x96 128x128 192x192 ; do
install -d -m0755                            %{buildroot}%{_datadir}/icons/hicolor/$dir/apps
install -p -m0644 $dir/AdobeReader9.png      %{buildroot}%{_datadir}/icons/hicolor/$dir/apps/
install -d -m0755                            %{buildroot}%{_datadir}/icons/hicolor/$dir/mimetypes
install -p -m0644 $dir/adobe.pdf.png         %{buildroot}%{_datadir}/icons/hicolor/$dir/mimetypes/
install -p -m0644 $dir/vnd.adobe.pdx.png     %{buildroot}%{_datadir}/icons/hicolor/$dir/mimetypes/
install -p -m0644 $dir/vnd.adobe.xdp+xml.png %{buildroot}%{_datadir}/icons/hicolor/$dir/mimetypes/
install -p -m0644 $dir/vnd.adobe.xfdf.png    %{buildroot}%{_datadir}/icons/hicolor/$dir/mimetypes/
install -p -m0644 $dir/vnd.fdf.png           %{buildroot}%{_datadir}/icons/hicolor/$dir/mimetypes/
done
popd
install -Dpm644 Shell/acroread.1.gz %{buildroot}%{_mandir}/man1/acroread.1.gz
rm -r Shell Support
popd
rm -r Browser/HowTo
rm    Browser/install_browser_plugin
rm    bin/UNINSTALL
ln -sf /etc/pki/tls/certs/ca-bundle.crt Reader/Cert/curl-ca-bundle.crt  
popd
# required for Adobe AIR
install -dm755 %{buildroot}/opt
ln -s %{_libdir}/acroread/Adobe %{buildroot}/opt

%files
%doc %{_libdir}/acroread/Adobe/Reader9/Reader/help/ENU/ReadMe.htm
%license Reader_Player_AIR_WWEULA-Combined*.pdf
%license %{_libdir}/acroread/Adobe/Reader9/Reader/Legal/en_US/*
%{_bindir}/acroread
%dir %{_libdir}/acroread
%dir %{_libdir}/acroread/Adobe
%dir %{_libdir}/acroread/Adobe/Reader9
%{_libdir}/acroread/Adobe/Reader9/Browser
%dir %{_libdir}/acroread/Adobe/Reader9/Reader
%{_libdir}/acroread/Adobe/Reader9/Reader/AcroVersion
%{_libdir}/acroread/Adobe/Reader9/Reader/Cert
%{_libdir}/acroread/Adobe/Reader9/Reader/GlobalPrefs
%dir %{_libdir}/acroread/Adobe/Reader9/Reader/help
%dir %{_libdir}/acroread/Adobe/Reader9/Reader/help/ENU
%{_libdir}/acroread/Adobe/Reader9/Reader/IDTemplates
%{_libdir}/acroread/Adobe/Reader9/Reader/intellinux
%{_libdir}/acroread/Adobe/Reader9/Reader/JavaScripts
%dir %{_libdir}/acroread/Adobe/Reader9/Reader/Legal
%dir %{_libdir}/acroread/Adobe/Reader9/Reader/Legal/en_US
%{_libdir}/acroread/Adobe/Reader9/Reader/PDFSigQFormalRep.pdf
%{_libdir}/acroread/Adobe/Reader9/Reader/pmd.cer
%{_libdir}/acroread/Adobe/Reader9/Reader/Tracker
%{_libdir}/acroread/Adobe/Reader9/Resource
%{_libdir}/acroread/Adobe/Reader9/bin
%{_datadir}/icons/hicolor/*/*
%{_datadir}/applications/AdobeReader.desktop
%{_mandir}/man1/acroread.1*
/opt/Adobe

%changelog
* Fri Nov 04 2022 Dominik Mierzejewski <rpm@greysector.net> - 9.5.5-7
- fix libldap/liblber soname detection

* Fri Apr 01 2022 Dominik Mierzejewski <rpm@greysector.net> - 9.5.5-6
- drop bash-completion script as it overrides _filedir and breaks directory
  completion

* Sun May 03 2020 Dominik Mierzejewski <rpm@greysector.net> - 9.5.5-5
- drop SynchronizerApp to avoid depending on obsolete pangox

* Tue Apr 14 2020 Dominik Mierzejewski <rpm@greysector.net> - 9.5.5-4
- bring back browser plugin, still useful for some apps
- add symlink to vendor default location for Adobe AIR

* Fri Nov 08 2019 Dominik Mierzejewski <rpm@greysector.net> - 9.5.5-3
- use system CA trust store
- don't duplicate ReadMe.htm and mark it as doc
- drop bundled xdg-* scripts
- declare bundled Myriad CAD font

* Wed Mar 14 2018 Dominik Mierzejewski <rpm@greysector.net> - 9.5.5-2
- modernize spec
- filter internal Provides:/Requires:
- drop browser plugin

* Wed Nov 13 2013 Dominik Mierzejewski <rpm@greysector.net> - 9.5.5-1
- Updated to release 9.5.5

* Wed Mar 23 2011 Dominik Mierzejewski <rpm@greysector.net> - 9.4.2-1
- Updated to release 9.4.2

* Fri May 14 2010 Dominik Mierzejewski <rpm@greysector.net> - 9.3.2-1
- Updated to release 9.3.2

* Fri Apr 09 2010 Dominik Mierzejewski <rpm@greysector.net> - 9.3.1-2
- Fixed bash_completion file location

* Sun Mar 21 2010 Dominik Mierzejewski <rpm@greysector.net> - 9.3.1-1
- Updated to release 9.3.1

* Tue Nov 17 2009 Dominik Mierzejewski <rpm@greysector.net> - 9.2-1
- Updated to release 9.2
- added bash-completion rules and manpage

* Mon Nov 16 2009 Dominik Mierzejewski <rpm@greysector.net> - 8.1.7-1
- updated to release 8.1.7
- updated EULA

* Thu Mar 26 2009 Dominik Mierzejewski <rpm@greysector.net> - 8.1.4-1
- Updated to release 8.1.4 (security fixes)
- Clear execstack from binaries

* Thu Jan 15 2009 Dominik Mierzejewski <rpm@greysector.net> - 8.1.3-2
- fixed build on EL-5

* Fri Dec 05 2008 Dominik Mierzejewski <rpm@greysector.net> - 8.1.3-1
- Updated to release 8.1.3
- added missing hicolor-icons-theme dep
- specfile cleanups

* Thu Aug 28 2008 Dominik Mierzejewski <rpm@greysector.net> - 8.1.2-1.SU1
- Updated to release 8.1.2-SU1.

* Tue Sep 18 2007 Dominik Mierzejewski <rpm@greysector.net> - 8.1.1-1
- Updated to release 8.1.1.

* Wed Jun 27 2007 Dominik Mierzejewski <rpm@greysector.net> - 7.0.9-1
- Fixed openldap deps on x86_64
- Removed redundant BR: perl
- Updated to release 7.0.9

* Wed Aug 23 2006 Dominik Mierzejewski <rpm@greysector.net> - 7.0.8-1
- Updated to release 7.0.8.
- Fixed PPKLite.api hidden dependency on openldap.

* Thu Jun 01 2006 Dominik Mierzejewski <rpm@greysector.net> - 7.0.1-1
- Updated to release 7.0.1.

* Fri Mar 18 2005 Dag Wieers <dag@wieers.com> - 7.0.0-2
- Disabled stripping globally to make plugin working. (Jürgen Möllenhoff)

* Tue Mar 15 2005 Dag Wieers <dag@wieers.com> - 7.0.0-1
- Updated to release 7.0.0.

* Mon Dec 20 2004 Dag Wieers <dag@wieers.com> - 5.0.10-1
- Feedback from Jason L Tibbitts.
- Updated to release 5.0.10.

* Thu Nov 18 2004 Dag Wieers <dag@wieers.com> - 5.0.9-2
- Removed %%{_libdir}/mozilla/plugins/

* Thu Jun 24 2004 Dag Wieers <dag@wieers.com> - 5.0.9-1
- Updated to release 5.0.9.
- Fixed the acroread icon. (Sahak Petrosyan)
- Added fix for crash when doing 'Find' on non-existing strings. (Stefan Hoelldampf)

* Tue Jan 27 2004 Dag Wieers <dag@wieers.com> - 5.0.8-1
- Added fix to make locale settings still work. (Fernando Lozano)

* Mon Jan 26 2004 Dag Wieers <dag@wieers.com> - 5.0.8-0
- Added exports for LANG and LC_ALL, just in case. (Axel Thimm)
- Updated to release 5.0.8.

* Mon Aug 25 2003 Dag Wieers <dag@wieers.com> - 5.0.7-2
- Fixed Unicode locale support, again. (Matthew Mastracci)

* Fri Aug 01 2003 Dag Wieers <dag@wieers.com> - 5.0.7-1
- Put mozilla plugins into a seperate package.
- Improved the desktop file.

* Sun Jul 13 2003 Dag Wieers <dag@wieers.com> - 5.0.7-0
- Initial package. (using DAR)
